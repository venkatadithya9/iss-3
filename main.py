import pygame
import random
import config
import math

pygame.init()
# creating the screen
screen = pygame.display.set_mode((800, 600))

pygame.display.set_caption("Jaws 100")
ship = pygame.image.load('boat.png')
pygame.display.set_icon(ship)

# Creating sharks list
clk = pygame.time.Clock()
sharkImg = []
sharkX = []
sharkY = []
sharkX_change = []
sharkY_change = []
num_of_enemies = 5

# Creating shark icon and start position

for i in range(num_of_enemies):
    sharkImg.append(pygame.image.load('rsz_1shark.png'))
    sharkX.append(random.randint(0, 720))
    sharkY.append(520 - i * 114)
    sharkX_change.append(0.1)
    sharkY_change.append(0)


def shark(x, y, k):
    screen.blit(sharkImg[k], (x, y))


# Creating player1

player1img = pygame.image.load('sailing.png')
player1X = 400
player1Y = 570
player1X_change = 0
player1Y_change = 0
p1point = 0
p1points = 0


def player1(x, y):
    screen.blit(player1img, (x, y))


# Creating player2

player2X = 400
player2Y = 20
player2X_change = 0
player2Y_change = 0
p2point = 0
p2points = 0


def player2(x, y):
    screen.blit(player1img, (x, y))


hazard = pygame.image.load('hazard.png')


def create(x, y):
    screen.blit(hazard, (x, y))


# Initialize score ------------------------------------------------------------------------------------------------
p1_score = 0
p2_score = 0
font = pygame.font.Font(config.text, 16)

textX = 10
textY1 = 10
textX2 = 700


def show_score1(x, y):
    score = font.render("P1 Score: " + str(p1_score), True, config.white)
    screen.blit(score, (x, y))


def show_score2(x, y):
    score = font.render("P2 Score: " + str(p2_score), True, config.white)
    screen.blit(score, (x, y))


# -------------------------------------------------------------------------------------------------------------------
# Collision function and game ended function

def Collided(enemy_x, enemy_y, player_x, player_y):
    distance = math.sqrt(math.pow(enemy_x - player_x, 2) + math.pow(enemy_y - player_y, 2))
    if distance < 33:
        return True
    else:
        return False


# Texts to be displayed and functions for achieving the same


text = pygame.font.Font(config.text, 32)


def level_ended():
    global p1level, player1X, player1Y, p1_score, t1
    global p2level, player2X, player2Y, p2_score, t2
    loop = 1
    while loop:
        screen.fill(config.black)
        if current == 0:
            over_text = text.render(" DEAD!", True, config.white)
            cont_text = text.render(" Press c to continue", True, config.white)
            screen.blit(over_text, (200, 250))
            screen.blit(cont_text, (200, 300))
            pygame.display.update()

        if current == 1:
            if p1_score > p2_score:
                declare_text = text.render(" WINNER: Player 1 ", True, config.white)
                screen.blit(declare_text, (200, 100))
            elif p1_score < p2_score:
                declare_text = text.render(" WINNER: Player 2 ", True, config.white)
                screen.blit(declare_text, (200, 100))
            elif p1_score == p2_score:
                declare_text = text.render(" SCORES TIED! ", True, config.white)
                screen.blit(declare_text, (200, 100))
                if t1 < t2:
                    time_text = text.render(" Player 1 Wins!", True, config.white)
                    screen.blit(time_text, (200, 175))
                if t1 > t2:
                    time_text = text.render(" Player 2 Wins!", True, config.white)
                    screen.blit(time_text, (200, 175))
                if t1 == t2:
                    time_text = text.render(" Draw Match!", True, config.white)
                    screen.blit(time_text, (200, 175))
                t1_text = text.render(" Player1 time:" + str(t1 // 40), True, config.white)
                t2_text = text.render(" Player1 time:" + str(t2 // 40), True, config.white)
                cont_text = text.render(" Press c to continue", True, config.white)
                screen.blit(cont_text, (200, 350))
                screen.blit(t1_text, (200, 250))
                screen.blit(t2_text, (200, 300))
            pygame.display.update()

        for event1 in pygame.event.get():
            if event1.type == pygame.KEYDOWN:
                if event1.key == pygame.K_c:
                    if current == 0:
                        p1level += 1
                        player1X = 400
                        player1Y = 570
                    if current == 1:
                        p2level += 1
                        player2X = 400
                        player2Y = 20
                    p1_score = 0
                    p2_score = 0
                    t1 = 0
                    t2 = 0
                    loop = 0
            pygame.display.update()


def level_won():
    global p1level, p1_score, player1Y, player1X, player1X_change, player1Y_change, t1
    global p2level, p2_score, player2X, player2Y, player2X_change, player2Y_change, t2
    loop = 1
    while loop:
        screen.fill(config.black)
        if current == 0:
            level_text = text.render(" Level Completed!", True, config.white)
            screen.blit(level_text, (250, 250))
            cont_text = text.render(" Press c to continue", True, config.white)
            screen.blit(cont_text, (250, 300))
            pygame.display.update()

        if current == 1:
            if p1_score > p2_score:
                declare_text = text.render(" WINNER: Player 1 ", True, config.white)
                screen.blit(declare_text, (200, 100))
            elif p1_score < p2_score:
                declare_text = text.render(" WINNER: Player 2 ", True, config.white)
                screen.blit(declare_text, (200, 100))
            elif p1_score == p2_score:
                declare_text = text.render(" SCORES TIED! ", True, config.white)
                screen.blit(declare_text, (200, 100))
                if t1 < t2:
                    time_text = text.render(" Player 1 Wins!", True, config.white)
                    screen.blit(time_text, (200, 175))
                if t1 > t2:
                    time_text = text.render(" Player 2 Wins!", True, config.white)
                    screen.blit(time_text, (200, 175))
                if t1 == t2:
                    time_text = text.render(" Draw Match!", True, config.white)
                    screen.blit(time_text, (200, 175))
                t1_text = text.render(" Player1 time:" + str(t1 // 40), True, config.white)
                t2_text = text.render(" Player1 time:" + str(t2 // 40), True, config.white)
                cont_text = text.render(" Press c to continue", True, config.white)
                screen.blit(cont_text, (200, 350))
                screen.blit(t1_text, (200, 250))
                screen.blit(t2_text, (200, 300))
        pygame.display.update()

        for event1 in pygame.event.get():
            if event1.type == pygame.KEYDOWN:
                if event1.key == pygame.K_c:
                    if current == 0:
                        p1level += 1
                        player1X = 400
                        player1Y = 570
                        player2Y_change = 0
                        player2X_change = 0
                    if current == 1:
                        p2level += 1
                        player2X = 400
                        player2Y = 20
                        player1Y_change = 0
                        player1X_change = 0
                    loop = 0
                    p1_score = 0
                    p2_score = 0
                    t1 = 0
                    t2 = 0
            pygame.display.update()


def level_display(player):
    score = font.render("Level " + str(player), True, config.white)
    screen.blit(score, (300, 10))


# Level setup
p1level = 1
p2level = 1
current = 0
# Timer setup
t1 = 0
t2 = 0
# Game loop
playing = True
while playing:

    screen.fill(config.blue)

    # Creating lines in between

    pygame.draw.line(screen, config.brown, (0, 600), (800, 600), 60)
    pygame.draw.line(screen, config.brown, (0, 486), (800, 486), 30)
    pygame.draw.line(screen, config.brown, (0, 372), (800, 372), 30)
    pygame.draw.line(screen, config.brown, (0, 258), (800, 258), 30)
    pygame.draw.line(screen, config.brown, (0, 144), (800, 144), 30)
    pygame.draw.line(screen, config.brown, (0, 0), (800, 0), 60)

    # Player1 movement

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            playing = False
        if (current % 2) == 0:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    player1X_change = -3
                if event.key == pygame.K_RIGHT:
                    player1X_change = 3
                if event.key == pygame.K_UP:
                    player1Y_change = -3
                if event.key == pygame.K_DOWN:
                    player1Y_change = 3
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    player1X_change = 0
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    player1Y_change = 0

        elif (current % 2) == 1:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    player2X_change = -3
                if event.key == pygame.K_RIGHT:
                    player2X_change = 3
                if event.key == pygame.K_UP:
                    player2Y_change = -3
                if event.key == pygame.K_DOWN:
                    player2Y_change = 3

            if event.type == pygame.KEYUP and (current % 2) == 1:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    player2X_change = 0
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    player2Y_change = 0

    if (current % 2) == 0:
        player1X += player1X_change
        player1Y += player1Y_change

        if player1X <= 0:
            player1X = 768
        elif player1X >= 768:
            player1X = 0

        if player1Y < 30:
            level_won()
            current = 1
            player1X = 400
            player1Y = 570

        player1(player1X, player1Y)

    elif (current % 2) == 1:
        player2X += player2X_change
        player2Y += player2Y_change

        if player2X <= 0:
            player2X = 768
        elif player2X >= 768:
            player2X = 0

        if player2Y > 570:
            level_won()
            current = 0
            player2X = 400
            player2Y = 20

        player2(player2X, player2Y)

    # Shark movement and collision check
    p2point = 0
    p1point = 0
    for i in range(num_of_enemies):
        if (current % 2) == 0:
            check = Collided(sharkX[i], sharkY[i], player1X, player1Y)
            if check:
                level_ended()
                current = 1
                player2X_change = 0
                player2Y_change = 0
            elif sharkY[i] > player1Y:
                p1point += 10
            sharkX[i] += sharkX_change[i]
            sharkX_change[i] = 1 + p1level
            if sharkX[i] >= 720:
                sharkX[i] = 0

        elif (current % 2) == 1:
            check = Collided(sharkX[i], sharkY[i], player2X, player2Y)
            if check:
                level_ended()
                current = 0
                player1X_change = 0
                player1Y_change = 0

            elif sharkY[i] < player2Y:
                p2point += 10
            sharkX[i] += sharkX_change[i]
            if sharkX[i] <= 0:
                sharkX_change[i] = 1 + p2level
            elif sharkX[i] >= 720:
                sharkX[i] = 0
                sharkX_change[i] = 1 + p2level

        shark(sharkX[i], sharkY[i], i)

    # Creating stationary obstacles and checking if there is collision
    p2points = 0
    p1points = 0
    create(300, 471)
    create(500, 471)
    create(200, 355)
    create(650, 355)
    create(100, 243)
    create(400, 243)
    create(600, 129)
    create(350, 129)

    if (current % 2) == 0:
        passed = [False for i in range(9)]
        check = Collided(300, 471, player1X, player1Y)
        if check:
            level_ended()
            current = 1
            player2X_change = 0
            player2Y_change = 0
        elif 471 > player1Y and not passed[1]:
            passed[1] = True
        check = Collided(500, 471, player1X, player1Y)
        if check:
            level_ended()
            current = 1
            player2X_change = 0
            player2Y_change = 0
        elif 471 > player1Y and not passed[2]:
            passed[2] = True
        check = Collided(200, 355, player1X, player1Y)
        if check:
            level_ended()
            current = 1
            player2X_change = 0
            player2Y_change = 0
        elif 355 > player1Y and not passed[3]:
            passed[3] = True
        check = Collided(650, 355, player1X, player1Y)
        if check:
            level_ended()
            current = 1
            player2X_change = 0
            player2Y_change = 0
        elif 355 > player1Y and not passed[4]:
            passed[4] = True
        check = Collided(100, 243, player1X, player1Y)
        if check:
            level_ended()
            current = 1
            player2X_change = 0
            player2Y_change = 0
        elif 243 > player1Y and not passed[5]:
            passed[5] = True
        check = Collided(400, 243, player1X, player1Y)
        if check:
            level_ended()
            current = 1
            player2X_change = 0
            player2Y_change = 0
        elif 243 > player1Y and not passed[6]:
            passed[6] = True
        check = Collided(600, 129, player1X, player1Y)
        if check:
            level_ended()
            current = 1
            player2X_change = 0
            player2Y_change = 0
        elif 129 > player1Y and not passed[7]:
            passed[7] = True
        check = Collided(350, 129, player1X, player1Y)
        if check:
            level_ended()
            current = 1
            player2X_change = 0
            player2Y_change = 0
            # still need to change the game_over text based on the situation
        elif 129 > player1Y and not passed[8]:
            passed[8] = True
        for i in range(9):
            if passed[i]:
                p1points += 5

        p1_score = p1points + p1point

    elif (current % 2) == 1:
        passed = [False for i in range(9)]
        check = Collided(300, 471, player2X, player2Y)
        if check:
            level_ended()
            current = 0
            player1X_change = 0
            player1Y_change = 0
        elif 471 < player2Y and not passed[1]:
            passed[1] = True
        check = Collided(500, 471, player2X, player2Y)
        if check:
            level_ended()
            current = 0
            player1X_change = 0
            player1Y_change = 0
        elif 471 < player2Y and not passed[2]:
            passed[2] = True
        check = Collided(200, 355, player2X, player2Y)
        if check:
            level_ended()
            current = 0
            player1X_change = 0
            player1Y_change = 0
        elif 355 < player2Y and not passed[3]:
            passed[3] = True
        check = Collided(650, 355, player1X, player1Y)
        if check:
            level_ended()
            current = 0
            player1X_change = 0
            player1Y_change = 0
        elif 355 < player2Y and not passed[4]:
            passed[4] = True
        check = Collided(100, 243, player2X, player2Y)
        if check:
            level_ended()
            current = 0
            player1X_change = 0
            player1Y_change = 0
        elif 243 < player2Y and not passed[5]:
            passed[5] = True
        check = Collided(400, 243, player2X, player2Y)
        if check:
            level_ended()
            current = 0
            player1X_change = 0
            player1Y_change = 0
        elif 243 < player2Y and not passed[6]:
            passed[6] = True
        check = Collided(600, 129, player2X, player2Y)
        if check:
            level_ended()
            current = 0
            player1X_change = 0
            player1Y_change = 0
        elif 129 < player2Y and not passed[7]:
            passed[7] = True
        check = Collided(350, 129, player2X, player2Y)
        if check:
            level_ended()
            current = 0
            player1X_change = 0
            player1Y_change = 0
            # still need to change the game_over text based on the situation
        elif 129 < player2Y and not passed[8]:
            passed[8] = True
        for i in range(9):
            if passed[i]:
                p2points += 5
        p2_score = p2points + p2point

    if (current % 2) == 0:
        level_display(p1level)
        t1 += 1
    elif (current % 2) == 1:
        level_display(p2level)
        t2 += 1

    show_score1(textX, textY1)
    show_score2(textX2, textY1)
    pygame.display.update()
    clk.tick(40)

pygame.quit()
